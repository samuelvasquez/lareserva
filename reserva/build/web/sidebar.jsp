
<%@page import="java.util.ArrayList" %>
<%@page import="reserva.models.Menu" %>
<%@page import="reserva.controllers.MenuCtrl" %>
<%
    MenuCtrl men_ctrl = new MenuCtrl();
    ArrayList listame = men_ctrl.listado();
%>
<!-- Sidebar -->
<div class="bg-dark border-dark" id="sidebar-wrapper">
    <div class="sidebar-heading">Seguimiento </div>

    <div class="text-center">
        <img style="padding: 1px; width: 150px; height: 100px;" 
             src="<%=ruta%>recursos/img/practicas.jpg"><br>

    </div>
    <div class="list-group list-group-flush">
        <%
            for (int i = 0; i < listame.size(); i++) {
                Menu me = (Menu) listame.get(i);
                String clase = "list-group-item list-group-item-action bg-dark";
                if (me.getCodigo().equals(menu)) {
                    clase = clase + " active_dark";
                }
        %>
        <a href="<%=ruta%><%=me.getUrl()%>" 
           class="<%=clase%>">
            <span class="<%=me.getIcono()%>" title="" aria-hidden="true"></span> <%=me.getNombre()%></a>
            <%}%>

    </div>
</div>
<!-- /#sidebar-wrapper -->
<style type="text/css">
    #sidebar-wrapper {
        color: white !important;
    }
    .list-group-flush a{
        color: white !important;
    }
</style>