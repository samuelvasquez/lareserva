
<%@page import="reserva.models.Persona"%>
<%@page import="reserva.controllers.PersonaCtrl"%>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <link rel="icon" href="../recursos/img/img1.jpg" sizes="32x32" type="image/jpg">
        <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css" media="all">
        <link href="../recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <!-- Icons -->
        <link href="../recursos/css/font-awesome.min.css" rel="stylesheet">
        <link href="../recursos/css/simple-line-icons.min.css" rel="stylesheet">
        <!-- Main styles for this application -->
        <link href="../recursos/css/style.css" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css" media="all">
        <link href="../recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <link href="../recursos/css/simple-sidebar.css" rel="stylesheet">
        <title> Practicante</title>
        <%! String pagina;%>
        <%
            String menu = "Persona", ruta = "../", buscar, mensaje = "", clase_ = "";
            Boolean mostrar_msg = false;
            String id,nombres,apellidos,dni,direccion,sexo,accion;
            
            buscar = request.getParameter("buscar");
            if (buscar == null) {
                buscar = "";
            }

            id = request.getParameter("id");
            if (id == null) {
                id = "";
            }

            nombres = request.getParameter("nombres");
            if (nombres == null) {
                nombres = "";
            }

            apellidos = request.getParameter("apellidos");
            if (apellidos == null) {
                apellidos = "";
            }

            dni = request.getParameter("dni");
            if (dni == null) {
                dni = "";
            }

            direccion = request.getParameter("direccion");
            if (direccion == null) {
                direccion = "";
            }
             sexo = request.getParameter("sexo");
            if (sexo == null) {
                sexo = "";
            }

            accion = request.getParameter("accion");
            if (accion == null) {
                accion = "";
            }
        %>
    </head>
    <body>
        <%@page import="reserva.models.Persona" %>
        <%@page import="reserva.controllers.PersonaCtrl"%>
        <%
            PersonaCtrl per_ctrl = new PersonaCtrl();
            if (accion.equals("SI")) {
                mostrar_msg = true;
                Persona per = new Persona();//id,nombres,apellidos,dni,direccion,sexo;
                per.setNombres(nombres);
                per.setApellidos(apellidos);
                per.setDni(dni);
                per.setDireccion(direccion);
                per.setSexo(sexo);
                if (id.equals("")) {
                    if (per_ctrl.add(per)) {
                        clase_="succes";
                        mensaje="<b>Correcto</b> No se logro el registr� correctamente";
                    } else {
                        clase_="danger";
                        mensaje="<b>Error!</b> no se pudo registrar curso";
                    }
                } else {
                    per.setId(id);
                    if (per_ctrl.update(per)) {
                        clase_="succes";
                        mensaje="<b>Correcto</b> el curso se actualiz� correctamente";
                    } else {
                        clase_="danger";
                        mensaje="<b>Error!</b> no se pudo actualizar curso";
                    }
                }
            }
            if(accion.equals("DEL")){
                if (per_ctrl.delete(id)) {
                        clase_="succes";
                        mensaje="<b>Correcto</b> el curso se elimin� correctamente";
                    } else {
                        clase_="danger";
                        mensaje="<b>Error!</b> no se pudo eliminar curso";
                    }
            }
            ArrayList listado = per_ctrl.listado(buscar);
        %>
        <div class="d-flex" id="wrapper">

            <!-- Sidebar -->
            <%@include file="../sidebar.jsp" %>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <%@include file="../navbar.jsp" %>

                <div class="container-fluid">
                    <div class="table-responsive">
                        <h1 class="display-4">Plan de trabajo</h1>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <a href="index.jsp" class="btn btn-outline-success"><span class="oi oi-loop-circular" title="" aria-hidden="true"></span> Actualizar</a>
                                <button onclick="javascript:new_or_edit('');" type="button" class="btn btn-outline-primary"><span class="oi oi-plus" title="" aria-hidden="true"></span> Nuevo</button>
                            </div>

                            <div class="col-12 col-md-6">
                            <form class="form-inline float-right" autocomplete="off" method="get" action="index.jsp">
                                <input class="form-control mr-sm-2" type="search" placeholder="Search"
                                       aria-label="Search" name="buscar" autofocus value="<%=buscar%>">
                                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
                            </form>
                        </div>

                        </div>
                        <br>

                        <table class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>                        
                                    <th scope="col">N�</th>
                                    <th scope="col">Nombres Apellidos</th>
                                    <th scope="col">carrera</th>
                                    <th scope="col">ciclo</th>
                                    <th scope="col">edad</th>
                                    <th scope="col">numero</th>                                  
                                   <th  scope="col" colspan="2" class="text-center">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                               

                                <tr>
                                    <td width="20">001</td>
                                    <td width="60">samuel vasquez barboza</td>
                                    <td width="70">ing sistemas</td>
                                    <td width="50">5</td>
                                    <td width="50">100</td>
                                    <td width="50">00000</td>

                                    <td width="60" class="text-center" title="Editar"><a href="javascript:new_or_edit('')"><span class="oi oi-pencil" title="Editar" aria-hidden="true" ></span></a></td>
                                    <td width="60" class="text-center" title="Borrar"><a href="index.jsp?accion=DEL&id="><span class="oi oi-trash" title="Eliminar" aria-hidden="true"></span></a></td>
                                </tr>
                                  <tr>
                                    <td width="20">002</td>
                                    <td width="60">jorge polo herrera</td>
                                    <td width="70">ing sistemas</td>
                                    <td width="50">6</td>
                                    <td width="50">1000</td>
                                    <td width="50">00220</td>

                                    <td width="60" class="text-center" title="Editar"><a href="javascript:new_or_edit('')"><span class="oi oi-pencil" title="Editar" aria-hidden="true" ></span></a></td>
                                    <td width="60" class="text-center" title="Borrar"><a href="index.jsp?accion=DEL&id="><span class="oi oi-trash" title="Eliminar" aria-hidden="true"></span></a></td>
                                </tr>
                             

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->

        </div>
                                <!-- Modal -->
        <div class="modal fade" id="modalform" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                <form name="myform" method="post" action="index.jsp">
                    <input type="hidden" name="accion" value="SI"/>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><span id="NewEdit"></span> Persona</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="FormularioNewEdit"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- /#wrapper -->

        <script src="../recursos/js/jquery-3.3.1.min.js"></script>
        <script src="../recursos/js/bootstrap.bundle.js"></script>
        <script src="../recursos/js/bootstrap.js"></script>
        <%
            if (mostrar_msg) {
        %>
        <div class="alert alert-<%=clase_%> alert_dinamic" id="alertMs" role="alert">
            <%=mensaje%>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
            <%}%>
            
            <script type="text/javascript">
            function new_or_edit(id) {

                if (id) {
                    $('#NewEdit').html("Editar");
                } else {
                    $('#NewEdit').html("Nuevo");
                }
                $("#modalform").modal({show: true});

                $('#FormularioNewEdit').html("Cargando...");

                $.ajax({
                    type: 'GET',
                    url: 'form.jsp',
                    data: {'id': id},
                    success: function (data) {
                        $('#FormularioNewEdit').html(data);
                    }
                });
            }
            $('.alert').alert();
        </script>

        <script>
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>

    </body>
</html>
