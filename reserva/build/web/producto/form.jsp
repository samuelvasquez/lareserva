<%@page import="reserva.models.Producto"%>
<%@page import="reserva.controllers.ProductoCtrl"%>
<%@page import="reserva.models.Tipo_producto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="reserva.controllers.Tipo_productoCtrl"%>
<div class="container">
    <%
        String id;
        id = request.getParameter("id");
    %>
    <input type="hidden" name="id" value="<%=id%>"/>
    <%
    Tipo_productoCtrl tp_ctrl =new Tipo_productoCtrl();
    ArrayList tipo_productos = tp_ctrl.listado(id);
    %>
</div>
    <label for="tipo_producto">Tipo Producto:</label>
    
    <select name="tipo_producto_id" required type="text" class="form-control" id="tipo_producto">
        
            <%
            for(int i=0; i<tipo_productos.size();i++){
            Tipo_producto tp =(Tipo_producto) tipo_productos.get(i);
            %>
         <option value="<%=tp.getId()%>"><%=tp.getNombre()%></option>
        <%}%>
        
    </select>    
<div class="form-group">
    <label for="nombre" >Nombre</label>
    <input type="text" class="form-control" id="nombre"  placeholder="" name="nombre" maxlength="60" required autofocus autocomplete="off" >
</div>
<div class="form-group">
    <label for="descripcion" >Descripcion</label>
    <input type="text" class="form-control" id="descripcion"  placeholder="" name="descripcion" maxlength="60" required autofocus autocomplete="off" >
</div>

<div class="form-row">
    <div class="form-group col-sm-6">
        <label for="precio_compra">Precio Compra</label>
        <input type="number" class="form-control" id="precio_compra" placeholder="" name="precio_compra" maxlength="10" required autofocus autocomplete="off" min="1">
    </div>
    <div class="form-group col-sm-6">
        <label for="stock">Stock</label>
        <input type="number" class="form-control" id="stock" placeholder="" name="stock" maxlength="10" required autofocus autocomplete="off" min="1">
    </div>
    <div class="form-group col-sm-6">
        <label for="precio_venta">Precio Venta</label>
        <input type="number" class="form-control" id="leccion" placeholder="" name="precio_venta" maxlength="60" required autofocus autocomplete="off" min="1">
    </div>
</div>

<div class="form-row">
    <legend class="col-form-label col-sm-2 pt-0">Estado</legend>
    <br>
    <div  class="col-sm-3">
        <div class="form-check">
            <input class="form-check-input" type="radio" name="estado" id="gridRadios1" value="1" required>
            <label class="form-check-label" for="estadoForm1">
                Activo
            </label>
        </div>
    </div>

    <div class="col-sm-7">
        <div class="form-check">
            <input class="form-check-input" type="radio" name="estado" id="gridRadios2" value="0" required>
            <label class="form-check-label" for="estadoForm0">
                Desactivado
            </label>
        </div>
    </div>

</div> 


</div>
<%
    if (!id.equals("")) {

%>
<%@page import="reserva.models.Producto" %>
<%@page import="reserva.controllers.ProductoCtrl" %>
<%        ProductoCtrl pro_ctrl = new ProductoCtrl();
    Producto pro = pro_ctrl.edit(id);
%>
<script>
    var form = document.myform;
    form.tipo_producto_id.value = <%=pro.getTipo_producto_id()%>;
    form.nombre.value = '<%=pro.getNombre()%>';
    form.descripcion.value = '<%=pro.getDescripcion()%>';
    form.precio_compra.value = '<%=pro.getPrecio_compra()%>';
    form.precio_venta.value = '<%=pro.getPrecio_venta()%>';
    form.estado.value = '<%=pro.getEstado()%>';
    form.stock.value = '<%=pro.getStock()%>';

</script>
<%}%>