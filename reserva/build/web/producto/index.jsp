<%-- 
    Document   : index
    Created on : 19-feb-2019, 17:32:30
    Author     : Cleiser
--%>


<%@page import="reserva.models.Producto"%>
<%@page import="reserva.controllers.ProductoCtrl"%>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="icon" href="../recursos/img/img1.jpg" sizes="32x32" type="image/jpg">
        <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css" media="all">
        <link href="../recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <!-- Icons -->
        <link href="../recursos/css/font-awesome.min.css" rel="stylesheet">
        <link href="../recursos/css/simple-line-icons.min.css" rel="stylesheet">
        <!-- Main styles for this application -->
        <link href="../recursos/css/style.css" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="../recursos/css/bootstrap.css" media="all">
        <link href="../recursos/css/open-iconic-bootstrap.css" rel="stylesheet">
        <link href="../recursos/css/simple-sidebar.css" rel="stylesheet">
        <title>PRINICIPAL | PRODUCTOS</title>
        <%! String pagina;%>
        <%
            String menu = "Producto", ruta = "../", buscar, mensaje = "", clase_ = "";
            boolean mostrar_msg = false;
            String id,tipo_producto_id ,nombre, descripcion, precio_compra, precio_venta, estado,foto,stock, accion;
            
            buscar = request.getParameter("buscar");
            if (buscar == null) {
                buscar = "";
            }
            id = request.getParameter("id");
            if (id == null) {
                id = "";
            }
            tipo_producto_id = request.getParameter("tipo_producto_id");
            if (tipo_producto_id == null) {
                tipo_producto_id = "";
            }
            nombre = request.getParameter("nombre");
            if (nombre == null) {
                nombre = "";
            }
            descripcion = request.getParameter("descripcion");
            if (descripcion == null) {
                descripcion = "";
            }
            precio_compra = request.getParameter("precio_compra");
            if (precio_compra == null) {
                precio_compra = "";
            }
            precio_venta = request.getParameter("precio_venta");
            if (precio_venta == null) {
                precio_venta = "";
            }
            estado = request.getParameter("estado");
            if (estado == null) {
                estado = "";
            }
             stock = request.getParameter("stock");
            if (stock == null) {
                stock = "";
            }
            foto = request.getParameter("foto");
            if (foto == null) {
                foto = "";
            }
            accion = request.getParameter("accion");
            if (accion == null) {
                accion = "";
            }
        %>
    </head>
    <body>
        <%@page import="reserva.models.Producto" %>
        <%@page import="reserva.controllers.ProductoCtrl"%>
        <%
            ProductoCtrl pro_ctrl = new ProductoCtrl();
            
            if (accion.equals("SI")) {
                mostrar_msg = true;
                Producto pro = new Producto();
                 
                pro.setTipo_producto_id(tipo_producto_id);
                pro.setNombre(nombre);
                pro.setDescripcion(descripcion);
                pro.setStock(stock);
                pro.setPrecio_compra(precio_compra);
                pro.setPrecio_venta(precio_venta);
                pro.setEstado(estado);
                if (id.equals("")) {
                    if (pro_ctrl.add(pro)) {
                        clase_="success";
                        mensaje="<b>Correcto!!!</b> el curso se registro correctamente";
                    } else {
                        clase_ = "danger";
                        mensaje = "<b>Error!!!</b> el curso no se registro correctamente";
                    }

                } else {
                    pro.setId(id);
                    if (pro_ctrl.update(pro)) {
                        clase_ = "success";
                        mensaje = "<b>Correcto!!!</b> El curso se autualizo correctamente";
                    } else {
                        clase_ = "danger";
                        mensaje = "<b>Error!!!</b> El curso no se actualizo correctamente";
                    }

                }

            }
            if(accion.equals("DEL")){
                if(pro_ctrl.delete(id)){
                     clase_="success";
                     mensaje="<b>Correcto!!!</b> El curso se elemino correctamente";
                    }  
                    else{
                         clase_="danger";
                     mensaje="<b>Error!!!</b> El curso no se pudo eleminar correctamente";
                    }                 
            }
            ArrayList listado = pro_ctrl.listado(buscar);
        %>
        <div class="d-flex" id="wrapper">

            <!-- Sidebar -->
            <%@include file="../sidebar.jsp" %>
            <!-- /#sidebar-wrapper -->

            <!-- Page Content -->
            <div id="page-content-wrapper">

                <%@include file="../navbar.jsp" %>

                <div class="container-fluid">
                    <div class="table-responsive">
                        <h1 class="display-4">Mis Productos</h1>
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <a href="index.jsp" class="btn btn-outline-success"><span class="oi oi-loop-circular" title="" aria-hidden="true"></span> Actualizar</a>
                                <button onclick="javascript:new_or_edit('');" type="button" class="btn btn-outline-primary"><span class="oi oi-plus" title="" aria-hidden="true"></span> Nuevo</button>
                            </div>

                            <div class="col-12 col-md-6" >
                                <form class="form-inline float-right" autocomplete="off" method="get" action="index.jsp" >
                                    <input class="form-control mr-sm-4" type="search" placeholder="Search" 
                                           aria-label="Search" name="buscar" autofocus value="<%=buscar%>">
                                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
                                </form>
                            </div>

                        </div>
                        <br>
                        <nav>
                            <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link" href="#">Ant</a>
                                </li>
                                <li class="page-item active">
                                    <a class="page-link" href="#">1</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">2</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">3</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">4</a>
                                </li>
                                <li class="page-item">
                                    <a class="page-link" href="#">Sig</a>
                                </li>
                            </ul>
                        </nav>
                        <br>
                        <table class="table table-bordered table-sm table-hover">
                            <thead>

                                <tr>
                                    <th scope="col">N�</th> 
                                    <th scope="col">Foto</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">descripcion</th>                                   
                                    <th scope="col">precio_compra</th>                           
                                    <th scope="col">stock</th>
                                    <th scope="col">precio_venta</th>
                                    <th scope="col">Foto</th>                                 
                                    <th scope="col">Estado</th>
                                    <th scope="col" colspan="2" class="text-center ">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    for (int i = 0; i < listado.size(); i++) {
                                        Producto pro = (Producto) listado.get(i);
                                %>
                                <tr>
                                    <td><%=i + 1%></td>
                                    <td width="30" aling="center">
                                    <% if(!pro.getFoto().equals("")){%>
                                    <img src="../recursos/img/productos/<%=pro.getFoto()%>" width="60"/>
                                    <%}%>
                                    </td>
                                    <td>
                                        <%=pro.getNombre()%><br>
                                       
                                    </td>
                                    <td><%=pro.getDescripcion()%></td>
                                    <td><%=pro.getPrecio_compra()%></td>

                                    <td><%=pro.getStock()%></td>
                                    <td><%=pro.getPrecio_venta()%></td>
                                    <td width="70" align="center">
                                        <a href="javascript:foto('<%=pro.getId()%>')"><span class="oi oi-image" title="foto" aria-hidden="true" ></span></a></td>

                                    <td>
                                        <span class="badge badge-success">Activo</span>
                                    </td>
                                    <td width="60" class="text-center"><a href="javascript:new_or_edit('<%=pro.getId()%>','<%=pro.getFoto()%>')"><span class="oi oi-pencil " title="Editar" aria-hidden="true"></span></a></td>
                                    <td width="60" class="text-center"><a href="index.jsp?accion=DEL&id=<%=pro.getId()%>"><span class="oi oi-trash " title="Borrar"   aria-hidden="true"></span></a></td>
                                </tr>
                                <%}%>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /#page-content-wrapper -->
        </div>
        <!-- Modal -->

        <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                <form name="myform" method="post" action="index.jsp">
                    <input type="hidden" name="accion" value="SI"/>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalScrollableTitle"><span id="NewEdit"></span>Producto <span id='abreviaturaCap'></span></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="FormularioNewEdit"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" >Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        
        
        
        <div class="modal fade" id="modalfoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
           <form name="myformfoto" method="post" action="subir.jsp" enctype="MULTIPART/FORM-DATA">
            <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
                
                    <input type="hidden" name="accion" value="SI"/>
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><span id="Newfoto"></span> Foto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div id="FormularioNewfoto"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                

            </div>
           </form>
        </div>
        <!-- /#wrapper -->

        <script src="../recursos/js/jquery-3.3.1.min.js"></script>
        <script src="../recursos/js/bootstrap.bundle.js"></script>
        <script src="../recursos/js/bootstrap.js"></script>

        <%
            if (mostrar_msg) {
        %>

        <div  class="alert alert-<%=clase_%> alert-dismissible fade alert_dinamic show" id="alertMs">
            <%=mensaje%>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <%}%>

        <script type="text/javascript">
            function new_or_edit(id) {

                if (id) {
                    $('#NewEdit').html("Editar");
                } else {
                    $('#NewEdit').html("Nuevo");
                }
                $("#modalForm").modal({show: true});

                $('#FormularioNewEdit').html("Cargando...");

                $.ajax({
                    type: 'GET',
                    url: 'form.jsp',
                    data: {'id': id},
                    success: function (data) {
                        console.log(data);
                        $('#FormularioNewEdit').html(data);
                    }
                });
            }
            //$('.alert').alert();

            function foto(id) {

                if (id) {
                    $('#Newfoto').html("Editar");
                } else {
                    $('#Newfoto').html("Nuevo");
                }
                $("#modalfoto").modal({show: true});

                $('#FormularioNewfoto').html("Cargando...");

                $.ajax({
                    type: 'GET',
                    url: 'foto.jsp',
                    data: {'id': id},
                    success: function (data) {
                        $('#FormularioNewfoto').html(data);
                    }
                });
            }
           // $('.alert').alert();
 
            $("#menu-toggle").click(function (e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
            window.setTimeout(function () {
                $('#alertMs').alert('close');
            }, 3000);
        </script>

    </body>
</html>
