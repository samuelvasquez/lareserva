<nav class="navbar navbar-expand-lg navbar-dark bg-primary " >
    <button class="btn btn-dark" id="menu-toggle"><span class="oi oi-menu" title="" aria-hidden="true"></span></button>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Mi Perfil
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item select_hover" href="#">Mi perfil</a>
                    <a class="dropdown-item select_hover" href="#">Cambiar comtrase�a</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item select_hover" href="#">Cerrar Sesi�n</a>
                </div>
            </li>
        </ul>
    </div>
</nav>
<style type="text/css">
    .select_hover:hover{
        background: #007bff;
        color: white;
    }
</style>