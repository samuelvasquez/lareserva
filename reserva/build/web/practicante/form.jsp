
<div class="container">
    <%
        String id;
        id = request.getParameter("id");
    %>
    <input type="hidden" name="id" value="<%=id%>"/>
    <div class="form-group">
        <label for="nombres" >Nombres</label>
        <input type="text" class="form-control" id="nombres"  placeholder="" name="nombres" maxlength="60" required autofocus autocomplete="off" >
    </div>
    <div class="form-group ">
        <label for="apellidos">Apellidos</label>
        <input type="text" class="form-control" id="apellidos" placeholder="" name="apellidos" maxlength="30" required  autocomplete="off">
    </div>
    <div class="form-group ">
        <label for="direccion">EP</label>
        <input type="text" class="form-control" id="direccion" placeholder="" name="direccion" maxlength="60" required  autocomplete="off" >
    </div>
    <div class="form-row">

        <div class="form-group col-sm-6">
            <label for="dni">Ciclo</label>
            <input type="text" class="form-control" id="dni" placeholder="" name="dni" maxlength="12" required autofocus autocomplete="off" min="1">
        </div>
        <div class="form-group col-sm-6">
            <label for="sexo">Facultad</label>
            <input type="text" class="form-control" id="sexo" placeholder="" name="sexo" maxlength="1" required autofocus autocomplete="off" min="1">
        </div>
        <div class="form-group col-sm-6">
            <label for="sexo">DNI</label>
            <input type="text" class="form-control" id="sexo" placeholder="" name="sexo" maxlength="1" required autofocus autocomplete="off" min="1">
        </div>
    </div>



</div>
<%
    if (!id.equals("")) {


%>
<%@page import="reserva.models.Persona" %>
<%@page import="reserva.controllers.PersonaCtrl" %>
<%        PersonaCtrl per_ctrl = new PersonaCtrl();
    Persona per = per_ctrl.edit(id);
%>
<script>
    var form = document.myform;
    form.nombres.value = '<%=per.getNombres()%>';
    form.apellidos.value = '<%=per.getApellidos()%>';
    form.dni.value = '<%=per.getDni()%>';
    form.direccion.value = '<%=per.getDireccion()%>';
    form.sexo.value = '<%=per.getSexo()%>';

</script>
<%}%>
