<%@page import="reserva.models.Detalle_venta"%>
<%@page import="reserva.controllers.Detalle_ventaCtrl"%>
<%@page import="reserva.models.Producto" %>
<%@page import="reserva.controllers.ProductoCtrl" %>
<%@page import="java.util.ArrayList"%>
<%@page import="reserva.models.Venta"%>
<%@page import="reserva.controllers.VentaCtrl"%>
<div class="container">
    <%
        String id;
        id = request.getParameter("id");
    %>
    <input type="hidden" name="id" value="<%=id%>"/>
    <%
        ProductoCtrl pro_ctrl = new ProductoCtrl();
        ArrayList producto = pro_ctrl.listado(id);
        VentaCtrl ven_ctrl = new VentaCtrl();
        ArrayList venta = ven_ctrl.listado(id);
    %>
</div>
<label for="tipo_producto">Producto 2:</label>

<select name="producto_id" required type="text" class="form-control" id="producto_id">

    <%
        for (int i = 0; i < producto.size(); i++) {
            Producto pro = (Producto) producto.get(i);
    %>
    <option value="<%=pro.getId()%>"><%=pro.getNombre()%></option>
    <%}%>

</select>    

<label for="tipo_producto">Venta</label>

<select name="venta_id" required type="text" class="form-control" id="venta_id">

    <%
        for (int i = 0; i < venta.size(); i++) {
            Venta ven = (Venta) venta.get(i);
    %>
    <option value="<%=ven.getId()%>"><%=ven.getCorrelativo()%></option>
    <%}%>

</select>
<div class="form-group">
    
</div>


<div class="form-row">
    <div class="form-group col-sm-6">
        <label for="precio">Precio </label>
        <input type="number" class="form-control" id="precio" placeholder="" name="precio" maxlength="10" required autofocus autocomplete="off" min="1">
    </div>
    <div class="form-group col-sm-6">
        <label for="cantidad">Cantidad</label>
        <input type="number" class="form-control" id="cantidad" placeholder="" name="cantidad" maxlength="60" required autofocus autocomplete="off" min="1">
    </div>
</div>




</div>
<%
    if (!id.equals("")) {

%>

<%        
    Detalle_ventaCtrl dv_ctrl = new Detalle_ventaCtrl();
    Detalle_venta dv = dv_ctrl.edit(id);
%>
<script>
    var form = document.myform;
    form.producto_id.value = '<%=dv.getProducto_id()%>';
    form.venta_id.value = '<%=dv.getVenta_id()%>';
    form.precio.value = '<%=dv.getPrecio()%>';
    form.cantidad.value = '<%=dv.getCantidad()%>';
    

</script>
<%}%>