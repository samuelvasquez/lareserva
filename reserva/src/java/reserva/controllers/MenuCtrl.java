/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reserva.controllers;

import java.util.ArrayList;
import reserva.models.Menu;

/**
 *
 * @author Cleiser
 */
public class MenuCtrl {
     public ArrayList listado() {
        ArrayList lista = new ArrayList();

        Menu men1 = new Menu();
        men1.setCodigo("Inicio");
        men1.setNombre("Inicio");
        men1.setIcono("oi oi-home");
        men1.setUrl("index.jsp");
        lista.add(men1);
      
        Menu men2 = new Menu();
        men2.setCodigo("Practicante");
        men2.setNombre("Practicante");
        men2.setIcono("oi oi-pie-chart");
        men2.setUrl("practicante/");
        lista.add(men2);
        
        Menu men3 = new Menu();
        men3.setCodigo("Plantrabajo");
        men3.setNombre("Plantrabajo");
        men3.setIcono("oi oi-pie-chart");
        men3.setUrl("plantrabajo/");
        lista.add(men3);
        
        Menu men4 = new Menu();
        men4.setCodigo("Formevaluacion");
        men4.setNombre("Formevaluacion");
        men4.setIcono("oi oi-pie-chart");
        men4.setUrl("formevaluacion/");
        lista.add(men4);

        return lista;
    }
    
}
