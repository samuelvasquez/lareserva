/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reserva.controllers;

import java.util.ArrayList;
import reserva.models.Persona;


/**
 *
 * @author Cleiser
 */
public class PersonaCtrl {
    public ArrayList listado(String buscar) {
        ArrayList lista = new ArrayList();
        Conexion cx = Configuracion.reserva();
        cx.execQuery("SELECT * FROM persona WHERE UPPER(nombres || dni|| apellidos) like UPPER('%"+buscar+"%') order by nombres, apellidos" );
        while (cx.getNext()) {
            //id,nombres,apellidos,dni,direccion,sexo;
            Persona per = new Persona();
            per.setId(cx.getCol("id"));
            per.setNombres(cx.getCol("nombres"));
            per.setApellidos(cx.getCol("apellidos"));
            per.setDni(cx.getCol("dni"));
            per.setDireccion(cx.getCol("direccion"));
            per.setSexo(cx.getCol("sexo"));

            lista.add(per);
        }
        return lista;
    }
    
     public boolean add(Persona per){
        Conexion cx = Configuracion.reserva();
        String com = "insert into persona(nombres,apellidos,dni,direccion,sexo) "
                + " values('"+per.getNombres()+"','"+per.getApellidos()+"', "
                + " '"+per.getDni()+"','"+per.getDireccion()+"','"+per.getSexo()+"')";
       
        try{
            cx.execC(com);
            cx.Commit();
            return true;
        }
        catch(Exception e){
            cx.RollBack();
            return false;
        }
    }
     public Persona edit(String id){
        Conexion cx = Configuracion.reserva();
        cx.execQuery("SELECT * FROM persona WHERE id="+id);
        cx.getNext();
          Persona per = new Persona();
            per.setId(cx.getCol("id"));
            per.setNombres(cx.getCol("nombres"));
            per.setApellidos(cx.getCol("Apellidos"));
            per.setDni(cx.getCol("dni"));
            per.setDireccion(cx.getCol("direccion"));
            per.setSexo(cx.getCol("sexo"));
           
            
            return per;
    }
     public boolean update(Persona per){
        Conexion cx = Configuracion.reserva();
        String com = "update persona set nombres='"+per.getNombres()+"', "
                + " apellidos='"+per.getApellidos()+"', "
                + " dni="+per.getDni()+", "
                + " direccion='"+per.getDireccion()+"', "
                + " sexo='"+per.getSexo()+"' "
                + " WHERE id="+per.getId();
        //System.out.println("VALOR: "+com);//imprimir error
       
        try{
            cx.execC(com);
            cx.Commit();
            return true;
        }catch (Exception e){
            cx.RollBack();
            return false;
        }
        }
     
     public boolean delete(String id ){
        Conexion cx = Configuracion.reserva();
        String com = "delete from persona WHERE id="+id;
       System.out.println("DATA: "+com);
        try{
            cx.execC(com);
            cx.Commit();
            return true;
        }catch (Exception e){
            cx.RollBack();
            return false;
        }
        }
    
    
}
