/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reserva.controllers;

import java.util.ArrayList;
import reserva.models.Detalle_venta;
import reserva.models.Producto;
import reserva.models.Venta;

/**
 *
 * @author Cleiser
 */
public class ProductoCtrl {

    public String traerPrecio(int id){
        String precio_venta = "";
        Conexion cx = Configuracion.reserva();
        cx.execQuery("SELECT precio_venta FROM producto WHERE id ="+id);
        while (cx.getNext()) {
            Producto pro = new Producto();
            pro.setPrecio_venta(cx.getCol("precio_venta"));
            precio_venta = pro.getPrecio_venta();
        }
        return precio_venta;
    }
    public Producto traerProductoPorId(int id){
        Conexion cx = Configuracion.reserva();
        cx.execQuery("SELECT * FROM producto WHERE id ="+id);
        cx.getNext();     
        Producto pro = new Producto();
        pro.setId(cx.getCol("id"));
        pro.setTipo_producto_id(cx.getCol("tipo_producto_id"));
        pro.setNombre(cx.getCol("nombre"));
        pro.setDescripcion(cx.getCol("descripcion"));
        pro.setPrecio_compra(cx.getCol("precio_compra"));
        pro.setPrecio_venta(cx.getCol("precio_venta"));
        pro.setEstado(cx.getCol("estado"));
        pro.setStock(cx.getCol("stock"));

        return pro;
    }
    
    
    public String registrarVenta(Venta vent, Detalle_venta det_vent) {      
        Conexion cx = Configuracion.reserva();        
        cx.execQuery("select public.registrar_venta("
                + "'"+vent.getTipo_comprobante_id()+"',"
                + "'"+vent.getCliente_id()+"',"
                + "'"+vent.getVendedor_id()+"',"
                + "'"+vent.getCorrelativo()+"', "
                + "'"+vent.getFecha()+"', "
                + "'"+vent.getTotal()+"',"
                + "'"+det_vent.getProducto_id()+"', "
                + "'"+det_vent.getPrecio()+"', "
                + "'"+det_vent.getCantidad()+"') resultado");
        cx.Commit();
        cx.getNext();
        String id = cx.getCol("resultado");
        
        return id;
    }
    
    public ArrayList listado(String buscar) {
        ArrayList lista = new ArrayList();
        Conexion cx = Configuracion.reserva();
        cx.execQuery("SELECT * FROM producto WHERE UPPER(nombre || tipo_producto_id) like UPPER('%" + buscar + "%') order by nombre, tipo_producto_id");
        while (cx.getNext()) {
            //id_producto,tipo_producto_id,precio_compra,descripcion,stock,precio_venta,foto,nombre,estado;
            Producto pro = new Producto();
            pro.setId(cx.getCol("id"));
            pro.setNombre(cx.getCol("nombre"));
            pro.setDescripcion(cx.getCol("descripcion"));
            pro.setTipo_producto_id(cx.getCol("tipo_producto_id"));
            pro.setPrecio_compra(cx.getCol("precio_compra"));
            pro.setStock(cx.getCol("stock"));
            pro.setPrecio_venta(cx.getCol("precio_venta"));
            pro.setFoto(cx.getCol("foto"));
            pro.setEstado(cx.getCol("estado"));
            lista.add(pro);
        }
        return lista;
    }

    public boolean add(Producto pro) {
        Conexion cx = Configuracion.reserva();
        String com = "insert into producto(nombre,descripcion,tipo_producto_id,precio_compra,stock,precio_venta,estado) "
                + " values('" + pro.getNombre() + "','" + pro.getDescripcion() + "'," + pro.getTipo_producto_id() + "," + pro.getPrecio_compra() + "," + pro.getStock() + "," + pro.getPrecio_venta() + ",'" + pro.getEstado() + "')";
        System.out.println("DATA: " + com);
        try {
            cx.execC(com);
            cx.Commit();
            return true;
        } catch (Exception e) {
            cx.RollBack();
            return false;
        }
    }

    public Producto edit(String id) {
        Conexion cx = Configuracion.reserva();
        cx.execQuery("SELECT * FROM producto WHERE id=" + id);
        cx.getNext();
        Producto pro = new Producto();
        pro.setId(cx.getCol("id"));
        pro.setTipo_producto_id(cx.getCol("tipo_producto_id"));
        pro.setNombre(cx.getCol("nombre"));
        pro.setDescripcion(cx.getCol("descripcion"));
        pro.setPrecio_compra(cx.getCol("precio_compra"));
        pro.setPrecio_venta(cx.getCol("precio_venta"));
        pro.setEstado(cx.getCol("estado"));
        pro.setStock(cx.getCol("stock"));

        return pro;
    }

    public boolean update(Producto pro) {
        Conexion cx = Configuracion.reserva();
        String com = "update producto set nombre='" + pro.getNombre() + "', "
                + " descripcion='" + pro.getDescripcion() + "', "
                + " precio_compra=" + pro.getPrecio_compra() + ", "
                + " precio_venta=" + pro.getPrecio_venta() + ", "
                + " estado='" + pro.getEstado() + "', "
                + " stock=" + pro.getStock() + " "
                + " WHERE id=" + pro.getId();
        //System.out.println("VALOR: "+com);//imprimir error
        try {
            cx.execC(com);
            cx.Commit();
            return true;
        } catch (Exception e) {
            cx.RollBack();
            return false;
        }
    }

    public boolean delete(String id) {
        Conexion cx = Configuracion.reserva();
        String com = "delete from producto WHERE id=" + id;
        //System.out.println("VALOR: "+com);
        try {
            cx.execC(com);
            cx.Commit();
            return true;
        } catch (Exception e) {
            cx.RollBack();
            return false;
        }
    }

    public boolean update_foto(String id, String nombre_foto) {
        Conexion cx = Configuracion.reserva();
        String com = "update producto set foto='" + nombre_foto + "' WHERE id=" + id;
        //System.out.println(com+"cc");

        try {
            cx.execC(com);
            cx.Commit();
            return true;
        } catch (Exception e) {
            cx.RollBack();
            return false;
        }
    }

}
