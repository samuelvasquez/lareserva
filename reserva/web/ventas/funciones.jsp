<%-- 
    Document   : nuevaventa
    Created on : 26-feb-2019, 20:05:18
    Author     : Cleiser
--%>

<%@page import="reserva.models.Detalle_venta"%>
<%@page import="reserva.models.Venta"%>
<%@page import="reserva.models.Producto"%>
<%@page import="reserva.controllers.ProductoCtrl"%>

<%
    String id;
    String opcion;
    String precio_producto;
    String datos_producto;
    
    boolean mostrar_msg = false;

    id = request.getParameter("id");
    opcion = request.getParameter("opcion");
    
    if (opcion == null) {
        opcion = "";
    }

    if (id == null) {
        id = "";
    }

    ProductoCtrl pro_ctrl = new ProductoCtrl();
    Producto pro = new Producto();
    int id_producto = Integer.parseInt(id);

    if (opcion.equalsIgnoreCase("precio")) {
        if (id != null) {
            precio_producto = pro_ctrl.traerPrecio(id_producto);
            out.println(precio_producto);
        }

    }

    if (opcion.equalsIgnoreCase("producto")) {
        if (id != null) {
            pro = pro_ctrl.traerProductoPorId(id_producto);
            /* lleno los datos una sola variable de tipo string, saparandolos por un "/" (slash) para dividirlos en el javascript */
            datos_producto = "" + pro.getId() + "/" + pro.getNombre() + "/" + pro.getPrecio_venta() + "";

            out.println(datos_producto);
        }

    }

    if (opcion.equalsIgnoreCase("registrar_venta")) {
        if (id != null) {
            out.println("hola");
        }
    }
%>

