<%-- 
    Document   : registrarme
    Created on : 26/02/2019, 05:48:29 PM
    Author     : Jared
--%>





<div class="form-group ">
    <label for="nombres">Nombres</label>
    <input class="form-control" id="nombres" placeholder="" 
           name="nombres" maxlength="60" autocomplete="off"  required>
</div>

<div class="form-row">
    <div class="form-group col-sm-6">
        <label for="papellido">Primer Apellido</label>
        <input name="papellido" class="form-control" maxlength="60" id="papellido" required >
    </div>
    <div class="form-group col-sm-6">
        <label for="sapellido">Segundo Apellido</label>
        <input name="sapellido" class="form-control" maxlength="60" id="sapellido">

    </div>
</div>

<div class="form-row">
    <div class="form-group col-sm-6">
        <label for="sexo">Sexo</label>
        <select name="sexo" required class="form-control" id="sexo">
            <option value="MAS">Masculino</option>
            <option value="FEM">Femenino</option>
        </select>
    </div>
    <div class="form-group col-sm-6">
        <label for="religion_id">Religion</label>
        <select name="religion_id" class="form-control"  id="religion_id">
            <option value="">Ninguna</option>
            <%
            for(int i=0; i<religiones.size(); i++){
                Religion rel = (Religion)religiones.get(i);
            
            %>
            <option value="<%=rel.getId()%>"><%=rel.getNombre()%></option>
            <%}%>
        </select>
    </div>
</div>


<div class="form-group ">
    <label for="direccion">Direccion</label>
    <input type="text" class="form-control" id="direccion" placeholder="" 
           name="direccion" maxlength="200" autocomplete="off"  required>
</div>

<div class="form-row">
    <div class="form-group col-sm-4">
        <label for="celular">Celular</label>
        <input name="celular" class="form-control" maxlength="30" id="celular"  >
    </div>
    <div class="form-group col-sm-8">
        <label for="correo">Correo</label>
        <input type="email" name="correo" class="form-control" maxlength="60" id="correo" >
    </div>
</div>

<blockquote class="blockquote">
    <p class="mb-0">Datos de acceso</p>
    <footer class="blockquote-footer">Ingrese usuario y contraseņa</footer>
</blockquote>

<div class="form-row">
    <div class="form-group col-sm-6">
        <label for="usuario">Usuario</label>
        <input name="usuario" class="form-control" maxlength="40" id="usuario" required >
    </div>
    <div class="form-group col-sm-6">
        <label for="contrasenia">Contraseņa</label>
        <input type="password" name="contrasenia" class="form-control" maxlength="30" id="contrasenia" required >
    </div>
</div>

<script type="text/javascript">

    var nombres = document.getElementById('nombres');
    window.setTimeout(function () {
        nombres.focus();
    }, 500);

    var form = document.myform;

</script>