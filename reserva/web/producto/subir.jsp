<%-- 
    Document   : subir
    Created on : 21-feb-2019, 16:13:31
    Author     : Cleiser
--%>

<%@page import="java.util.regex.Pattern"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="org.apache.commons.fileupload.disk.*"%>
<%@page import="org.apache.commons.fileupload.servlet.*"%>
<%@page import="org.apache.commons.io.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="reserva.controllers.ProductoCtrl"%>


<%
    String server = getServletContext().getRealPath("/").replace('\\', '/').replace("build/web/", "");

    String direccion = server + "web/recursos/img/productos/";
    FileItemFactory file_factory = new DiskFileItemFactory();

    ServletFileUpload servlet_up = new ServletFileUpload(file_factory);

    List items = servlet_up.parseRequest(request);
    String id_producto = "";
    for (int i = 0; i < items.size(); i++) {
        FileItem item = (FileItem) items.get(i);

        if (item.isFormField() && item.getFieldName().equals("id")) {
            id_producto = item.getString();
            break;

        }
    }

    for (int i = 0; i < items.size(); i++) {
        FileItem item = (FileItem) items.get(i);
        if (!item.isFormField()) {
            String[] dividir = item.getName().split(Pattern.quote("."));
            String extension = dividir[dividir.length-1];

            UUID uuid = UUID.randomUUID();
            String nombre = uuid.toString().replace("-", "");

            File archivo_server = new File(direccion+""+nombre+"."+extension);
            item.write(archivo_server);

            ProductoCtrl pro_ctrl = new ProductoCtrl();
            if (pro_ctrl.update_foto(id_producto, nombre+"."+extension)) {
                out.println("El Archivo guardado correctamente...");
            } else {
                out.println("El Archivo no se guardo correctamente...");
            }    

%>

<META  http-equiv="refresh" content="2;url=index.jsp"/>

<%
        }
    }
%>

